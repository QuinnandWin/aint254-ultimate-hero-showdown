﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Skill : MonoBehaviour
{

    //Variables to tell what type of move it is: Does it target enemy, does it boost
    //any stast etc.
    public bool enemyTarget = true;
    public bool modifyAttackerStats = false;
    public bool modifyDefenderStats = false;

    //Variables of the skill, affecting power of move, and if it targets allies or
    //enemies, and if it boosts/weakens a stat and its nature
    public float power = 5.0f;
    public float attackModifier = 1.0f;
    public float hpModifier = 0.0f;
    public float maxHpModifier = 0.0f;
    public float natureModifier = 1.0f;
    public float baseCooldown = 0.0f;
    float currentCooldown = 0.0f;
    public string nature = "water";
    public int targetCount = 1;
    public List<GameObject> targets;

    //Stats of attacker and defender, created here and later set to vallues in methods
    protected float hp = 0.0f;
    protected float startHp = 0.0f;
    protected float attack = 1.0f;
    protected float defense = 1.0f;
    protected float speed = 0.0f;
    protected float damageDone= 0.0f;
    protected string defenderNature;
    protected bool targeting = false;
    protected int targetsRemaining = 3;

    //A count to ensure that a debuffs to the character are only done once no matter how many
    //targets there are 
    protected int skillUseCount=0;

    //The gameObjects required are the person attacking and the target(s)
    public GameObject attacker;
    public string attackerName;
    protected GameObject defender;
    protected GameObject[] heroesArray;

    //Reference to skill button, this is so it can later not be clicked if cooldown is set
    public Button skillButton;

    //
    private GameObject TurnManagerScript;
    private GameObject TeamTargets;
    //private GameObject ChooseImages;

    //Bool parameters that tell the program whether the player can target themselves or the enemy
    bool selfTarget = false;
    public bool friendlyTargets = false;


    //Audio paramters, the sound effect that is played when a skill is used
    public AudioClip skillSoundClip;
    AudioSource skillAudio;

    protected void Start()
    {
        TurnManagerScript = GameObject.Find("SkillUI");
        TeamTargets = GameObject.Find("TargetsUI");
        skillAudio = TurnManagerScript.GetComponent <AudioSource>();

    }

    protected void Awake()
    {
        heroesArray = new GameObject[6];
    }

    protected void OnEnable()
    {
        EventManager.StartListening("Cooldown", LowerCooldown);
    }

    protected void OnDisable()
    {
        EventManager.StopListening("Cooldown", LowerCooldown);
    }

    // Use this for initialization
    public void SkillStart()
    {

        print("Start!");
        if (currentCooldown <= 0)
        {
            currentCooldown = 0;

            targeting = true;

            //Remove previous targetting so that if player is switching between abilities targetting does not appear over
            //both allies and enemies
            TeamTargets.transform.GetChild(1).gameObject.SetActive(false);
            TeamTargets.transform.GetChild(2).gameObject.SetActive(false);
       

            if (selfTarget == false)
            {
                if (friendlyTargets == false)
                {
                    if (attacker.GetComponent<HeroStats>().teamNumber == 1)
                    {
                        targetsRemaining = TurnManagerScript.GetComponent<GlobalValues>().teamTwoRemaining;

                    }
                    else if (attacker.GetComponent<HeroStats>().teamNumber == 2)
                    {
                        targetsRemaining = TurnManagerScript.GetComponent<GlobalValues>().teamOneRemaining;
                    }
                }
                else if (friendlyTargets == true)
                {
                    if (attacker.GetComponent<HeroStats>().teamNumber == 2)
                    {
                        targetsRemaining = TurnManagerScript.GetComponent<GlobalValues>().teamTwoRemaining;

                    }
                    else if (attacker.GetComponent<HeroStats>().teamNumber == 1)
                    {
                        targetsRemaining = TurnManagerScript.GetComponent<GlobalValues>().teamOneRemaining;
                    }
                }

                if (targetCount > targetsRemaining)
                {
                    targetCount = targetsRemaining;
                }

                SkillSelected();

                targets = new List<GameObject>();
                ChooseTarget.currentSkill = this;
            }
            else
            {
                defenseSkill();
                //ChooseImages.GetComponent<UnableUIElement>().disableElement();
            }
        }
        else
        {
            Debug.Log("Can't use move it's in cooldown");
            Debug.Log(currentCooldown);
        }
    }

    public void SkillSelected()
    {
        int attackingTeamNumber = attacker.GetComponent<HeroStats>().teamNumber;
        int i = 0;
        for (i = 0; i < 6; i++)
        {
            heroesArray[i] = TurnManagerScript.GetComponent<TurnManager>().heroes[i] as GameObject;
        }
        for (i = 0; i < 6; i++)
        {
            //Stops healing skills targetting enemies
            if (friendlyTargets == true)
            {
                //Unable Enemy colliders
                if (attackingTeamNumber != heroesArray[i].GetComponent<HeroStats>().teamNumber)
                {
                    heroesArray[i].GetComponent<BoxCollider2D>().enabled = false;
                }
                //Enable friendly colliders
                else if (attackingTeamNumber == heroesArray[i].GetComponent<HeroStats>().teamNumber)
                {
                    TeamTargets.transform.GetChild(attackingTeamNumber).gameObject.SetActive(true);
                    heroesArray[i].GetComponent<BoxCollider2D>().enabled = true;
                }
            }

            //Stops attacks from targetting allies
            if (friendlyTargets == false)
            {
                //Unable friendly colliders
                if (attackingTeamNumber == heroesArray[i].GetComponent<HeroStats>().teamNumber)
                {
                    heroesArray[i].GetComponent<BoxCollider2D>().enabled = false;
                }
                //Enable enemy colliders
                else if (attackingTeamNumber != heroesArray[i].GetComponent<HeroStats>().teamNumber)
                {
                    if (attackingTeamNumber == 1)
                    {
                        TeamTargets.transform.GetChild(2).gameObject.SetActive(true);
                    }
                    else if (attackingTeamNumber == 2)
                    {
                        TeamTargets.transform.GetChild(1).gameObject.SetActive(true);
                    }
                    heroesArray[i].GetComponent<BoxCollider2D>().enabled = true;
                }
            }
        }
        i= 0;
    }
    
    public void AddTarget(GameObject newTarget)
    {
        print(newTarget.name);
        bool exists = targets.Contains(newTarget);
        if (!exists)
        {
            targets.Add(newTarget);
        }
    }

    // Update is called once per frame
    protected void Update()
    {
        if (attacker.GetComponent<HeroStats>().HeroHp <= 0)
        {
            TurnManagerScript.GetComponent<TurnManager>().TurnOrder();
            attacker.GetComponent<HeroTurn>().stopTurn();
        }
        if (targeting && targets.Count >= targetCount)
        {
            Debug.Log("End");
            targeting = false;

            TeamTargets.transform.GetChild(1).gameObject.SetActive(false);
            TeamTargets.transform.GetChild(2).gameObject.SetActive(false);

            //Turns off glow around skills after they have been perfromed so the illusion
            //of it being selected is not kept

            TurnManagerScript.GetComponent<Glow>().TurnOffGlow();
            //ChooseImages.GetComponent<UnableUIElement>().disableElement();
            //Now targets have been selected and the move is being processed the
            //sound of the skill plays
            if (skillSoundClip != null)
            {
                Debug.Log("Sound Performed");
                skillAudio.clip = skillSoundClip;
                skillAudio.Play();
            }

            for (int i = 0; i < targetCount; i++)
            {
                defender = targets[i];

                DoSkill();
            }

            TurnManagerScript.GetComponent<TurnManager>().TurnOrder();
            attacker.GetComponent<HeroTurn>().stopTurn();
        }

    }

    protected void DoSkill()
    {

        if (enemyTarget == true)
        {
            attackSkill();
        }
        else
        {
            defenseSkill();
        }

        EventManager.TriggerEvent("Cooldown");
    }

    protected void attackSkill()
    {
        
            IsNatureModifier();

            attack = attacker.GetComponent<HeroStats>().HeroAttack;

            speed = attacker.GetComponent<HeroStats>().HeroSpeed;

            hp = defender.GetComponent<HeroStats>().HeroHp;
            startHp = defender.GetComponent<HeroStats>().HeroStartHp;

        //if friendly target don't do damage calculations
        if (friendlyTargets != true)
        {
            //Debug.Log("Attackers attack value : " + attack);
            damageDone = ((power + attack) * natureModifier);
            if (damageDone < 1)
            {
                damageDone = 1;
            }
            print("Power + Attack = " + (power + attack));
            print("Nature Modifier = " + natureModifier);
            print("Damage Done = " + damageDone);
            hp = hp - damageDone;
        }
            hp = Mathf.Round(hp);
            //Debug.Log("Defenders remaining HP: " + hp);
            defender.GetComponent<HeroStats>().HeroHp = hp;
            HealthText.currentHealth = hp;
        

        if (hp <= 0)
        {
            if (defender.GetComponent<HeroStats>().teamNumber == 1)
            {
                targetsRemaining = TurnManagerScript.GetComponent<GlobalValues>().teamOneRemaining;
                targetsRemaining = targetsRemaining - 1;
                TurnManagerScript.GetComponent<GlobalValues>().teamOneRemaining = targetsRemaining;
                if (targetsRemaining<=0)
                {
                    TurnManagerScript.GetComponent<GlobalValues>().Team2Win();
                    Debug.Log("Team 1 Eliminated, Team 2 WINS!");
                }
            }
            else if (defender.GetComponent<HeroStats>().teamNumber == 2)
            {
                targetsRemaining = TurnManagerScript.GetComponent<GlobalValues>().teamTwoRemaining;
                targetsRemaining = targetsRemaining - 1;
                TurnManagerScript.GetComponent<GlobalValues>().teamTwoRemaining = targetsRemaining;
                if (targetsRemaining <= 0)
                {
                    TurnManagerScript.GetComponent<GlobalValues>().Team1Win();
                    Debug.Log("Team 2 Eliminated, Team 1 WINS!");
                }
            }

        }

        if (modifyAttackerStats == true)
        {
            if (skillUseCount < 1)
            {
                attack = attacker.GetComponent<HeroStats>().HeroAttack;
                attack = attack + attackModifier;
                attacker.GetComponent<HeroStats>().HeroAttack = attack;

                startHp = attacker.GetComponent<HeroStats>().HeroStartHp;
                startHp = startHp + maxHpModifier;
                attacker.GetComponent<HeroStats>().HeroStartHp = startHp;

                hp = attacker.GetComponent<HeroStats>().HeroHp;
                hp = hp + hpModifier;
                hp = Mathf.Round(hp);
                attacker.GetComponent<HeroStats>().HeroHp = hp;

                skillUseCount++;
                Invoke("ResetSkillUseCount", 0.5f);
            }
        }

        if (modifyDefenderStats == true)
        {
                attack = defender.GetComponent<HeroStats>().HeroAttack;
                attack = attack + attackModifier;
                defender.GetComponent<HeroStats>().HeroAttack = attack;

                startHp = defender.GetComponent<HeroStats>().HeroStartHp;
                startHp = startHp + maxHpModifier;
                defender.GetComponent<HeroStats>().HeroStartHp = startHp;

                hp = defender.GetComponent<HeroStats>().HeroHp;
                hp = hp + hpModifier;
                hp = Mathf.Round(hp);
                defender.GetComponent<HeroStats>().HeroHp = hp;

        }

        setCooldown();

    }

    protected void defenseSkill()
    {
        //Now targets have been selected and the move is being processed the
        //sound of the skill plays
        if (skillSoundClip != null)
        {
            Debug.Log("Sound Performed");
            skillAudio.clip = skillSoundClip;
            skillAudio.Play();
        }

        if (modifyAttackerStats == true)
        {
            attack = attacker.GetComponent<HeroStats>().HeroAttack;
            attack = attack + attackModifier;
            attacker.GetComponent<HeroStats>().HeroAttack = attack;

            startHp = attacker.GetComponent<HeroStats>().HeroStartHp;
            startHp = startHp + maxHpModifier;
            attacker.GetComponent<HeroStats>().HeroStartHp = startHp;

            hp = attacker.GetComponent<HeroStats>().HeroHp;
            hp = hp + hpModifier;
            hp = Mathf.Round(hp);
            attacker.GetComponent<HeroStats>().HeroHp = hp;


        }

        if (modifyDefenderStats == true)
        {
            attack = defender.GetComponent<HeroStats>().HeroAttack;
            attack = attack + attackModifier;
            defender.GetComponent<HeroStats>().HeroAttack = attack;

            startHp = defender.GetComponent<HeroStats>().HeroStartHp;
            startHp = startHp + maxHpModifier;
            defender.GetComponent<HeroStats>().HeroStartHp = startHp;

            hp = defender.GetComponent<HeroStats>().HeroHp;
            hp = hp + hpModifier;
            hp = Mathf.Round(hp);
            defender.GetComponent<HeroStats>().HeroHp = hp;
        }

        EventManager.TriggerEvent("Cooldown");

        TurnManagerScript.GetComponent<TurnManager>().TurnOrder();
        attacker.GetComponent<HeroTurn>().stopTurn();

        setCooldown();
    }

    protected void setCooldown()
    {
        currentCooldown = baseCooldown;

        if (currentCooldown > 0)
        {
            skillButton.interactable = false;
            print(skillButton);
        }
    }

    private void LowerCooldown()
    {
        currentCooldown = currentCooldown - 1;

        if (currentCooldown <= 0)
        {
            skillButton.interactable = true;
        }
    }

    protected void IsNatureModifier()
    {
        defenderNature = defender.GetComponent<HeroStats>().HeroNature;

        if (nature == "water")
        {
            if (defenderNature == "fire")
            {
                natureModifier = 2.01f;
            }
            else if (defenderNature == "electric")
            {
                natureModifier = 0.49f;
            }
            else
            {
                natureModifier = 1;
            }
        }

        else if (nature == "fire")
        {
            if (defenderNature == "earth")
            {
                natureModifier = 2.01f;
            }
            else if (defenderNature == "water")
            {
                natureModifier = 0.49f;
            }
            else
            {
                natureModifier = 1;
            }
        }

        else if (nature == "earth")
        {
            if (defenderNature == "electric")
            {
                natureModifier = 2.01f;
            }
            else if (defenderNature == "fire")
            {
                natureModifier = 0.49f;
            }
            else
            {
                natureModifier = 1;
            }
        }

        else if (nature == "electric")
        {
            if (defenderNature == "water")
            {
                natureModifier = 2.01f;
            }
            else if (defenderNature == "earth")
            {
                natureModifier = 0.49f;
            }
            else
            {
                natureModifier = 1;
            }
        }
    }

    void ResetSkillUseCount()
    {
        //reset skillUseCount
        skillUseCount = 0;
    }

    /*protected void isSkillSelected(bool skillIsSelected)
    {
        if (skillIsSelected == true)
        {
            skillIsSelected = false;
        }
        if (skillIsSelected == false)
        {
            skillIsSelected = true;
        }
    }*/
}
