﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class TurnManager : MonoBehaviour {

    HeroTurn takeTurnReference;
    int x = 0;

    public GameObject[] heroes;
    /*public GameObject middleLeftHero;
    public GameObject bottomLeftHero;
    public GameObject topRightHero;
    public GameObject middleRightHero;
    public GameObject bottomRightHero;*/

    float[] heroSpeeds= new float[6];

    int[] order = new int[6];

    //Round counter text
    public Text roundText;
    int roundCounter=0;

    // Use this for initialization
    void Start () {
        
        //Set speeds to array
        for (int i=0; i<heroSpeeds.Length; i++)
        {
            heroSpeeds[i] = heroes[i].GetComponent<HeroStats>().HeroSpeed;
        }

        //Sort array by value
        Array.Sort(heroSpeeds);

        //This now produces the order so the final array named order should say which hero is
        //first/last in this case it says 5,3,1 meaning 5 is last 3 is second last and so on
        x = 0;
        while (x < 6)
        {
            for (int i = 0; i < heroSpeeds.Length; i++)
            {
                if (heroSpeeds[x] == heroes[i].GetComponent<HeroStats>().HeroSpeed)
                {
                    order[x] = i;
                    //Debug.Log(order[x]);
                }
            }
            x++;
        }
        x = 5;

        TurnOrder();


    }
	
    public void TurnOrder()
    {
        if (x >= 5)
        {
            RecheckSpeed();
            RoundCounterChange();
        }

        takeTurnReference = heroes[order[x]].GetComponent<HeroTurn>();
        takeTurnReference.takeTurn();

        if (x <= 0)
        {
            x = 6;
        }
        x = x - 1;
    }

    void RecheckSpeed()
    {
        //Set speeds to array
        for (int i = 0; i < heroSpeeds.Length; i++)
        {
            heroSpeeds[i] = heroes[i].GetComponent<HeroStats>().HeroSpeed;
        }

        //Sort array by value
        Array.Sort(heroSpeeds);

        //This now produces the order so the final array named order should say which hero is
        //first/last in this case it says 5,3,1 meaning 5 is last 3 is second last and so on
        x = 0;
        while (x < 6)
        {
            for (int i = 0; i < heroSpeeds.Length; i++)
            {
                if (heroSpeeds[x] == heroes[i].GetComponent<HeroStats>().HeroSpeed)
                {
                    order[x] = i;
                    //Debug.Log(order[x]);
                }
            }
            x++;
        }

        x = 5;
    }

    void RoundCounterChange()
    {
        roundCounter++;
        roundText.text = "round " + roundCounter;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
