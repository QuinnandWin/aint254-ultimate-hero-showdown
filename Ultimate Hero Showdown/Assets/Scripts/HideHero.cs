﻿using UnityEngine;
using System.Collections;

public class HideHero : MonoBehaviour {

    public SpriteRenderer heroSprite;
    public GameObject hero;
    public GameObject heroSkills;

    SpriteRenderer secondHeroSprite;
    GameObject secondHero;
    GameObject secondHeroSkills;

    public bool beginHide = false;

    // Use this for initialization
    void Start()
    {
        if (beginHide == true)
        {
            HideAll();
        }
    }

    void HideAll()
    {
        //heroSprite.enabled = false;
        hero.SetActive(false);
        //heroSkills.SetActive(false);
    }

    void ShowAll()
    {
        heroSprite.enabled = true;
        hero.SetActive(true);
        heroSkills.SetActive(true);
    }

    void SecondHideAll()
    {
        secondHeroSprite.enabled = false;
        secondHero.SetActive(false);
        secondHeroSkills.SetActive(false);
    }

    void SecondShowAll()
    {
        secondHeroSprite.enabled = true;
        secondHero.SetActive(true);
        secondHeroSkills.SetActive(true);
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        print("Mouse entered trigger");
        HideAll();
    }

    /*void ChangeSecondToFirst()
    {
        heroSprite = secondHeroSprite;
        hero = secondHero;
        heroSkills = secondHeroSkills;
    }*/

    // Update is called once per frame
    void Update()
    {

    }
}
