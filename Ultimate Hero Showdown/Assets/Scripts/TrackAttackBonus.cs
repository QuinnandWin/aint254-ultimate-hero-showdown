﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TrackAttackBonus : MonoBehaviour {

    public GameObject heroToTrack;
    float heroAttackBonus;
    public Text heroAttackBonusText;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        heroAttackBonus = heroToTrack.GetComponent<HeroStats>().HeroAttack;
        if (heroAttackBonus >= 0)
        {
            heroAttackBonusText.text = "+" + heroAttackBonus;
        }
        else if (heroAttackBonus < 0)
        {
            heroAttackBonusText.text = "" + heroAttackBonus;
        }

    }
}
