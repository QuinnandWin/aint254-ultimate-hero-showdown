﻿using UnityEngine;
using System.Collections;

public class HeroStats : MonoBehaviour {

    public float HeroHp = 1.0f;
    public float HeroStartHp = 1.0f;
    public float HeroAttack = 1.0f;
    public float HeroSpeed = 0.0f;
    public string HeroNature = "water";
    public int teamNumber = 1;

    // Use this for initialization
    void Start () {
        gameObject.GetComponent<HealthText>().Awake();
	}
	
	// Update is called once per frame
	void Update () {

        if (HeroHp > HeroStartHp)
        {
            HeroHp = HeroStartHp;
        }

        if (HeroHp <= 0)
        {
            HeroHp = 0;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }

        if (HeroAttack < -2)
        {
            HeroAttack = -2;
        }

        if (HeroAttack > 3)
        {
            HeroAttack = 3;
        }
    }

}
