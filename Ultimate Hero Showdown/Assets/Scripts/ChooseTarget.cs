﻿using UnityEngine;
using System.Collections;

public class ChooseTarget : MonoBehaviour
{
    public static Skill currentSkill;
    public bool addingHeroTarget=true;
    //bool skillSelectionTime = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
	}

    void OnTriggerEnter2D(Collider2D target)
    {
        if (addingHeroTarget == true)
        {
            currentSkill.AddTarget(target.gameObject);
        }
        else
        {

        }
        /*else if (skillSelectionTime == false)
        {
            //changeHero();
        }*/
    }
}
