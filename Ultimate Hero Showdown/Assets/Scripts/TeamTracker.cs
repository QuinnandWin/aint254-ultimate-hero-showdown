﻿using UnityEngine;
using System.Collections;

public class TeamTracker : MonoBehaviour {

    public string[] Team1;
    public string[] Team2;
    bool team1Choosing=true;
    int teamNumberTracking = 0;
    int i = 0;


    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    public void AddHeroString(string heroString)
    {
        if (i >= 3)
        {
            team1Choosing = false;
            i = 0;
        }

        if (team1Choosing == true)
        {
            Team1[i] = heroString;
            i++;
        }
        else if (team1Choosing == false)
        {
            Team2[i] = heroString;
            i++;
        }

    }
}
