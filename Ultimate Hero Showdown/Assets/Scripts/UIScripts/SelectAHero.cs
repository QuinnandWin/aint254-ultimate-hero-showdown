﻿using UnityEngine;
using System.Collections;

public class SelectAHero : MonoBehaviour {

    public GameObject heroInformation;
    public GameObject heroSquareSprite;
    GameObject heroSquareSpriteSpawned;
    public RectTransform[] heroSelectPositions;
    public RectTransform[] team2HeroSelectPositions;
    public GameObject parentObject;
    GameObject sceneCanvas;
    GameObject currentlySelectedHero;
    GameObject mousePosition;
    public bool team1Choosing = true;
    int i = 0;

    public GameObject teamTrackerObject;
    public string selectAHeroString;

    public GameObject createHeroesObject;

    // Use this for initialization
    void Start () {
        sceneCanvas = GameObject.Find("HeroSelectTextCanvas");
        mousePosition = GameObject.Find("MousePosition");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D target)
    {
        team1Choosing = sceneCanvas.GetComponent<CurrentSelected>().team1Choosing;
        if (team1Choosing == false)
        {
            gameObject.GetComponent<FlipHero>().Flip();
        }
        sceneCanvas.GetComponent<CurrentSelected>().currentSelected.SetActive(false);

        sceneCanvas.GetComponent<CurrentSelected>().currentSelected = null;
        sceneCanvas.GetComponent<CurrentSelected>().currentSelected = heroInformation;
        
        sceneCanvas.GetComponent<CurrentSelected>().currentSelected.SetActive(true);
        
    }

    void OnTriggerExit2D()
    {
    }

    public void ConfirmHeroSelection()
    {
        i = sceneCanvas.GetComponent<CurrentSelected>().heroSelectNumber;
        team1Choosing = sceneCanvas.GetComponent<CurrentSelected>().team1Choosing;
        print(i);
        
        if (team1Choosing == true)
        {
            heroSquareSpriteSpawned = Instantiate(heroSquareSprite, heroSelectPositions[i].position, heroSelectPositions[i].rotation) as GameObject;
            heroSquareSpriteSpawned.transform.SetParent(parentObject.transform);
            heroSquareSpriteSpawned.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
        else if (team1Choosing == false)
        {
            heroSquareSpriteSpawned = Instantiate(heroSquareSprite, team2HeroSelectPositions[i].position, heroSelectPositions[i].rotation) as GameObject;
            heroSquareSpriteSpawned.transform.SetParent(parentObject.transform);
            heroSquareSpriteSpawned.transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        }
        heroSquareSpriteSpawned.transform.SetParent(parentObject.transform);
        

        sceneCanvas.GetComponent<CurrentSelected>().heroSelectNumber++;

        teamTrackerObject.GetComponent<TeamTracker>().AddHeroString(selectAHeroString);

        if (i>=2 && team1Choosing==true)
        {
            gameObject.GetComponent<FlipHero>().ChangeInformation();
            i = 0;
            sceneCanvas.GetComponent<CurrentSelected>().heroSelectNumber = 0;
            team1Choosing = false;
            sceneCanvas.GetComponent<CurrentSelected>().team1Choosing = false;
        }
        if (i >= 2 && team1Choosing == false)
        {
            Application.LoadLevel("BattlePrototype");
        }


        Exit();
    }

    void Exit()
    {
        sceneCanvas.GetComponent<CurrentSelected>().currentSelected.SetActive(false);
        Invoke("ResetMousePosition", 0.3f);
    }

    void ResetMousePosition()
    {
        mousePosition.transform.localPosition = new Vector3(4.0f, 1.0f, 1.0f);
    }
}
