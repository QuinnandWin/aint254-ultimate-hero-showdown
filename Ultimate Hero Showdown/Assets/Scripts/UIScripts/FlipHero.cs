﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FlipHero : MonoBehaviour {

    public GameObject heroSprite;
    public GameObject heroText;
    public GameObject heroSelector;
    public Text informationText;
    int flipCount = 0;
	
	public void Flip()
    {
        if (flipCount <= 0)
        {
            heroSprite.transform.localScale = new Vector3(-heroSprite.transform.localScale.x, heroSprite.transform.localScale.y, heroSprite.transform.localScale.z);
            heroSprite.transform.localPosition = new Vector3(-heroSprite.transform.localPosition.x, heroSprite.transform.localPosition.y, heroSprite.transform.localPosition.z);
            heroText.transform.localPosition = new Vector3(490f , heroText.transform.localPosition.y, heroText.transform.localPosition.z);
            heroSelector.transform.localPosition = new Vector3(-heroSelector.transform.localPosition.x, heroSelector.transform.localPosition.y, heroSelector.transform.localPosition.z);
            flipCount++;

        }
    }

    public void ChangeInformation()
    {
        informationText.text = "player 2 choose your heroes";
    }
}
