﻿using UnityEngine;
using System.Collections;

public class MenuNaviagationManager : MonoBehaviour {

    string currentMusicName = "BackgroundMusic";
    string newUniqueMusicName= "NotDestroyedBackgroundMusic";
    GameObject originalMusic;

    public void LoadHeroSelectStart()
    {
        Application.LoadLevel("HeroSelectScreen");
    }

    public void LoadMainMenu()
    {
        Application.LoadLevel("Main Menu");
    }

    public void LoadBattle()
    {
        Application.LoadLevel("BattlePrototype");
    }

    public void ChangeMusicName()
    {
        originalMusic = GameObject.Find(currentMusicName);
        originalMusic.name = newUniqueMusicName;
    }
    // Update is called once per frame
    void Update () {
	
	}
}
