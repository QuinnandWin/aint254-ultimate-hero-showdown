﻿using UnityEngine;
using System.Collections;

public class UnableUIElement : MonoBehaviour {

    public GameObject UIElement;
    public GameObject UIElement2;

    public void enableElement()
    {
        if (UIElement != null)
        {
            UIElement.SetActive(true);
        }
        if (UIElement2 != null)
        {
            UIElement2.SetActive(false);
        }

    }

    public void disableElement()
    {
        if (UIElement != null)
        {
            UIElement.SetActive(false);
        }
        if (UIElement2 != null)
        {
            UIElement2.SetActive(true);
        }
    }
}
