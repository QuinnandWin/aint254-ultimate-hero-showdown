﻿using UnityEngine;
using System.Collections;

public class Glow : MonoBehaviour {

    public GameObject[] allGlows;

    public void TurnOffGlow()
    {
        int i = 0;
        while (i < 4)
        {
            allGlows[i].SetActive(false);
            i++;
        }
    }

    public void TurnOnGlow1()
    {
        TurnOffGlow();
        allGlows[0].SetActive(true);
    }

    public void TurnOnGlow2()
    {
        TurnOffGlow();
        allGlows[1].SetActive(true);
    }

    public void TurnOnGlow3()
    {
        TurnOffGlow();
        allGlows[2].SetActive(true);
    }

    public void TurnOnGlow4()
    {
        TurnOffGlow();
        allGlows[3].SetActive(true);
    }
}
