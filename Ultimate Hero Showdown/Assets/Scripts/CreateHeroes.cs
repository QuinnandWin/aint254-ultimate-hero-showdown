﻿using UnityEngine;
using System.Collections;

public class CreateHeroes : MonoBehaviour {

    public GameObject spriteParentObject;
    public GameObject skillsParentObject;
    public GameObject heroParentObject;
    public GameObject textParentObject;

    public GameObject magmaHero;
    GameObject magmaHeroObject;
    public GameObject magmaSkills;
    GameObject magmaSkillsObject;
    public GameObject magmaSprite;
    GameObject magmaSpriteObject;
    public GameObject magmaText;
    GameObject magmaTextObject;

    public GameObject[] heroTransforms;
    int i = 0;
    bool team1Choosing=true;

    GameObject teamTrackerObject;

    // Use this for initialization
    void Start () {

        teamTrackerObject = GameObject.Find("TeamTracker");
        AddHero();
        print("add hero");

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void AddHero()
    {
        while (team1Choosing == true)
        {
            if (i >= 3)
            {
                team1Choosing = false;
                i = 0;
            }
            while (i < 3 && team1Choosing == true)
            {
                if (teamTrackerObject.GetComponent<TeamTracker>().Team1[i] == "magma")
                {
                    AddMagma();
                    print("add magma");
                }
                i++;
            }
            while (i < 3 && team1Choosing == false)
            {
                if (teamTrackerObject.GetComponent<TeamTracker>().Team2[i] == "magma")
                {
                    AddMagma();
                }
                i++;
            }
        }
    }

    void AddMagma()
    {
        if (team1Choosing == true)
        {
            magmaHeroObject = Instantiate(magmaHero, heroTransforms[i].transform.position, heroTransforms[i].transform.rotation) as GameObject; 
            magmaSpriteObject = Instantiate(magmaSprite, heroTransforms[i].transform.position, heroTransforms[i].transform.rotation) as GameObject; 
            magmaTextObject = Instantiate(magmaText, heroTransforms[i].transform.position, heroTransforms[i].transform.rotation) as GameObject; 
            magmaText.transform.localPosition = new Vector3(magmaText.transform.localPosition.x, magmaText.transform.localPosition.y + 30f, magmaText.transform.localPosition.z);
            magmaSkillsObject = Instantiate(magmaSkills) as GameObject;

            magmaHeroObject.transform.SetParent(heroParentObject.transform);
            magmaSpriteObject.transform.SetParent(spriteParentObject.transform);
            magmaTextObject.transform.SetParent(textParentObject.transform);
            magmaTextObject.transform.localScale = new Vector3(0.4f, 0.4f, 1.0f);
            magmaTextObject.transform.localPosition = new Vector3(magmaTextObject.transform.localPosition.x, magmaTextObject.transform.localPosition.y + 40f, magmaTextObject.transform.localPosition.z);
            magmaSkillsObject.transform.SetParent(skillsParentObject.transform);

        }
        else if (team1Choosing == false)
        {
            magmaHeroObject = Instantiate(magmaHero, heroTransforms[i+3].transform.position, heroTransforms[i + 3].transform.rotation) as GameObject;
            magmaSpriteObject = Instantiate(magmaSprite, heroTransforms[i + 3].transform.position, heroTransforms[i + 3].transform.rotation) as GameObject;
            magmaTextObject = Instantiate(magmaText, heroTransforms[i + 3].transform.position, heroTransforms[i + 3].transform.rotation) as GameObject;
            magmaText.transform.localPosition = new Vector3(magmaText.transform.localPosition.x, magmaText.transform.localPosition.y + 30f, magmaText.transform.localPosition.z);
            magmaSkillsObject = Instantiate(magmaSkills) as GameObject;

            magmaHeroObject.transform.SetParent(heroParentObject.transform);
            magmaSpriteObject.transform.SetParent(spriteParentObject.transform);
            magmaTextObject.transform.SetParent(textParentObject.transform);
            magmaTextObject.transform.localScale = new Vector3(0.4f, 0.4f, 1.0f);
            magmaTextObject.transform.localPosition = new Vector3(magmaTextObject.transform.localPosition.x, magmaTextObject.transform.localPosition.y + 40f, magmaTextObject.transform.localPosition.z);
            magmaSkillsObject.transform.SetParent(skillsParentObject.transform);

            magmaSpriteObject.transform.localScale = new Vector3(-magmaSprite.transform.localScale.x, magmaSprite.transform.localScale.y, magmaSprite.transform.localScale.z);
        }
    }
}
