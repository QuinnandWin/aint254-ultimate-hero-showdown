﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthText : MonoBehaviour {

    public static float currentHealth = 0;        // The hero's health.
    float maxHealth = 0;

    GameObject textObject;
    public string textName;
    public Text text;                      // Reference to the Text component.

    GameObject hero;        //Which hero health is being changed

    int setHealthCounter = 0;

    public GameObject enemyTarget;


    public void Awake()
    {

        hero = gameObject;

        // Reset the health.
        maxHealth = hero.GetComponent<HeroStats>().HeroHp;
        currentHealth = maxHealth;

    }

    // Use this for initialization
    void Start()
    {
        textObject = GameObject.Find(textName);
        text = textObject.GetComponent<Text>();
    }


    void Update()
    {
        currentHealth = hero.GetComponent<HeroStats>().HeroHp;
        maxHealth = hero.GetComponent<HeroStats>().HeroStartHp;

        // Set the displayed text to be the current health followed by the max health.
        text.text = "hp - " + currentHealth + "/" + maxHealth;


        if (text == null)
        {
            print(this.name);
        }



        if (currentHealth <= 0)
        {
            currentHealth = 0;
            
            text.enabled = false;

            Destroy(enemyTarget);
        }
    }
}
