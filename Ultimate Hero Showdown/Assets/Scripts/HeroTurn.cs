﻿using UnityEngine;
using System.Collections;

public class HeroTurn : MonoBehaviour {

    public GameObject skills;

    public void takeTurn()
    {
        Debug.Log("turn active");
        skills.SetActive(true);
    }

    public void stopTurn()
    {
        skills.SetActive(false);
    }
    
}
