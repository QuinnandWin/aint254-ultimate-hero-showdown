﻿using UnityEngine;
using System.Collections;

public class DoNotDestroyOnLoad : MonoBehaviour {

    public static bool doDestroy = false;

    //
    public string previousNotDestroyedName;
    public bool destroyPrevious = false;
    private GameObject previous;
    //Current name of object that can compared to later
    public string currentName;

    //Settable different name for object so Unity can tell difference between
    //two exact copies of each other
    public string newUniqueName;

    //Reference to copy of the gameObject
    private GameObject copy;

    //Number of times the objects has been loaded
    private int timesLoaded = 0;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    // Object attached to this script will now not be destroyed on the load of a new level
    void Update()
    {
        if (gameObject.name != currentName)
        {
            copy = GameObject.Find(currentName);
            Destroy(copy);

            //Searches for copy 3 times before stopping the update, this is to
            //ensure it searches it the scene that it is changing too also
            if (copy != null || timesLoaded>=2)
            {
                gameObject.name = currentName;
                timesLoaded = 0;
            }
            else
            {
                timesLoaded++;
            }
        }

        if (destroyPrevious == true)
        {
            Debug.Log("Destroy previous");
            previous = GameObject.Find(previousNotDestroyedName);
            Destroy(previous);

            destroyPrevious = false;
        }

        if (doDestroy == true)
        {
            Destroy(gameObject);
        }

        if (doDestroy == false)
        {
            gameObject.SetActive(true);
        }
    }

    public void ChangeName()
    {
        gameObject.name = newUniqueName;

    }

}
