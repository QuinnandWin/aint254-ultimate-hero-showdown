﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GlobalValues : MonoBehaviour {

    public int teamOneRemaining = 3;
    public int teamTwoRemaining = 3;
    public GameObject winnerUI;
    public Text Winnertext;

    // Use this for initialization
    void Start () {

        teamOneRemaining = 3;
        teamTwoRemaining = 3;
    }
	
	// Update is called once per frame
	public void Team1Win()
    {
        winnerUI.SetActive(true);
        Winnertext.text = "TEAM ONE!";
    }

    public void Team2Win()
    {
        winnerUI.SetActive(true);
        Winnertext.text = "TEAM TWO!";
    }
}
